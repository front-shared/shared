class EventBusClass {
  #listeners: { [eventName: string]: ((data: any) => void)[] } = {};

  addEventListener(eventName: string, callback: (data: any) => void): void {
    if (this.#listeners[eventName]) {
      this.#listeners[eventName].push(callback);
      return;
    }

    this.#listeners[eventName] = [callback];
  }

  hasEvent(eventName: string): boolean {
    return !!this.#listeners[eventName];
  }

  dispatchEvent(type: string, data: any): void {
    const noEventListeners = !this.#listeners[type] || this.#listeners[type].length == 0;
    if (noEventListeners) return;
    this.#listeners[type].forEach((callback) => {
      callback(data);
    })
  }
}

export const EventBus = new EventBusClass();
