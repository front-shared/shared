export interface LogOptions {
  showLocation?: boolean,
  minLevels: {
    [moduleName: string]: number,
  }
}
