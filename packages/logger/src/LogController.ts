import { LOG_LEVEL } from './LogLevel.js';
import { LogOptions } from './LogOptions.js';
import { LogEntry } from './LogEntry.js';
import { Logger } from './Logger.js' ;
import { EventBus } from '@frontconnect/eventbus';

class LogControllerClass {
  #options: LogOptions = {
    showLocation: true,
    minLevels: {
      '': LOG_LEVEL.info,
      }
  }

  configure(logOptions?: LogOptions): LogControllerClass {
    if (!logOptions) return this;

    this.#options = {
      ...this.#options,
      ...logOptions,
    }
    return this;
  }

  get(moduleName: string): Logger {
    const minLevel = this.#options.minLevels[moduleName];
    if (minLevel) return new Logger(moduleName, minLevel, this.#options.showLocation);

    return new Logger(moduleName, this.#options.minLevels[''], this.#options.showLocation);
  }

  private log(logEntry: LogEntry): void {
    if (logEntry.logLevel == LOG_LEVEL.trace) {
      console.trace(logEntry.location, 'trace', `[${logEntry.moduleName}]`, ...logEntry.messages);
    } else if (logEntry.logLevel == LOG_LEVEL.debug) {
      console.debug(logEntry.location, 'debug', `[${logEntry.moduleName}]`, ...logEntry.messages);
    } else if (logEntry.logLevel == LOG_LEVEL.info) {
      console.info(logEntry.location, 'info', `[${logEntry.moduleName}]`, ...logEntry.messages);
    } else if (logEntry.logLevel == LOG_LEVEL.warn) {
      console.warn(logEntry.location, 'warn', `[${logEntry.moduleName}]`, logEntry.messages);
    } else if (logEntry.logLevel == LOG_LEVEL.error) {
      console.error(logEntry.location, 'error', `[${logEntry.moduleName}]`, ...logEntry.messages);
    } else {
      console.log(`{${logEntry.logLevel}}`, ...logEntry.messages);
    }
  }

  register() {
    if (EventBus.hasEvent('log')) return;
    EventBus.addEventListener('log', this.log.bind(this));
  }
}

export const LogController = new LogControllerClass();
