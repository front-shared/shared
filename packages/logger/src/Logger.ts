import { LOG_LEVEL } from './LogLevel.js';
import { LogEntry } from './LogEntry.js';
import { EventBus } from '@frontconnect/eventbus';

export class Logger {
  readonly #minLevel: number = LOG_LEVEL.trace;
  readonly #showLocation: boolean;
  readonly #moduleName: string;

  constructor(moduleName: string, minLevel: number, showLocation: boolean | undefined) {
    this.#moduleName = moduleName;
    this.#minLevel = minLevel;
    this.#showLocation = showLocation ?? false;
  }

  private log(logLevel: number, ...messages: any[]): void {
    if (logLevel < this.#minLevel) return;
    const logEntry: LogEntry = {
      logLevel: logLevel,
      moduleName: this.#moduleName,
      messages,
    };

    // Obtain the line/file through a thoroughly hacky method
    // This creates a new stack trace and pulls the caller from it.
    if (this.#showLocation) {
      const error = new Error('');
      if (error.stack) {
        const lines = error.stack.split('\n');
        const locationIndex = lines.findIndex((line) => {
          return (line.includes(`at Logger.${LOG_LEVEL[logEntry.logLevel]}`))
        });
        logEntry.location = (lines[locationIndex + 1]).trim();
      }
    }

    EventBus.dispatchEvent('log', logEntry);
  }

  public trace(...messages: any[]): void {
    this.log(LOG_LEVEL.trace, ...messages);
  }
  public debug(...messages: any[]): void {
    this.log(LOG_LEVEL.debug, ...messages);
  }
  public info(...messages: any[]): void  {
    this.log(LOG_LEVEL.info, ...messages);
  }
  public warn(...messages: any[]): void  {
    this.log(LOG_LEVEL.warn, ...messages);
  }
  public error(...messages: any[]): void {
    this.log(LOG_LEVEL.error, ...messages);
  }
}

