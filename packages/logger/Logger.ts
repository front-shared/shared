import { LogController } from './src/LogController.js';
import { LogOptions } from './src/LogOptions.js';
import { Logger } from './src/Logger.js';

class FcLoggerClass {
  configure(logOptions?: LogOptions): void {
    LogController
      .configure(logOptions)
      .register();
  }

  get(moduleName: string): Logger {
    return LogController.get(moduleName);
  }
}

export const FCLogger = new FcLoggerClass();
